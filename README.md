# Install Kubrentetes to AWS use kops
**Linux

## Getting Started

---
## Install aws client

> `pip install awscli`

---
## Install kubectl

> `wget -O kubectl https://storage.googleapis.com/kubernetes-release/release/v1.12.2/bin/linux/amd64/kubectl`

> `chmod +x ./kubectl`

> `sudo mv ./kubectl /usr/local/bin/kubectl`

---
## Install kops

> `wget -O kops https://github.com/kubernetes/kops/releases/download/1.10.0/kops-linux-amd64`

> `chmod +x ./kops`

> `sudo mv ./kops /usr/local/bin/`

---
## Setup your environment

create group "kops" with permition

* AmazonEC2FullAccess

* AmazonRoute53FullAccess

* AmazonS3FullAccess

* IAMFullAccess

* AmazonVPCFullAccess

and add user "kops"

You can create the kops IAM user from the command line using the following:

> `aws iam create-group --group-name kops`


> `aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess --group-name kops`

> `aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonRoute53FullAccess --group-name kops`

> `aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --group-name kops`

> `aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/IAMFullAccess --group-name kops`

> `aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess --group-name kops`


> `aws iam create-user --user-name kops`

> `aws iam add-user-to-group --user-name kops --group-name kops`

> `aws iam create-access-key --user-name kops`

__configure the aws client to use your new IAM user__

> `aws configure           # Use your new access and secret key here`

> `aws iam list-users      # you should see a list of all your IAM users here`

__Because "aws configure" doesn't export these vars for kops to use, we export them now__

> `export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id)`
> `export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key)`

## Configure DNS
---
Note: If you are using Kops 1.6.2 or later, then DNS configuration is optional. Instead, a gossip-based cluster can be easily created. 

The only requirement to trigger this is to have the cluster name end with .k8s.local. 

If a gossip-based cluster is created then you can skip this section.

In order to build a Kubernetes cluster with kops, we need to prepare somewhere to build the required DNS records. 

__How configure dns you can [read](https://github.com/kubernetes/kops/blob/master/docs/aws.md)__ 


## Cluster State storage (bucket)
---

> `aws s3api create-bucket \`

>    `--bucket prefix-example-com-state-store \`

>    `--region us-east-1`

add versioning your S3 bucket 

> `aws s3api put-bucket-versioning --bucket prefix-example-com-state-store  --versioning-configuration Status=Enabled`


## Creating your first cluster
---

We're ready to start creating our first cluster! Let's first set up a few environment variables to make this process easier.

> `export NAME=myfirstcluster.example.com`

> `export KOPS_STATE_STORE=s3://prefix-example-com-state-store`

For a gossip-based cluster, make sure the name ends with k8s.local. For example:

> `export NAME=myfirstcluster.k8s.local`

> `export KOPS_STATE_STORE=s3://prefix-example-com-state-store`

Note: You don't have to use environmental variables here. You can always define the values using the name and state flags later.


## Create cluster configuration
---

We will need to note which availability zones are available to us. In this example we will be deploying our cluster to the us-west-2 region.

> `aws ec2 describe-availability-zones --region eu-central-1`

> `kops create cluster --master-count 1 --node-count 2 --node-size t2.medium --zones eu-central-1a ${NAME}`

## Customize Cluster Configuration
---

> `kops edit cluster ${NAME}`


## Build the Cluster
---

> `kops update cluster ${NAME} --yes`

## Use the Cluster
---

> `kubectl get nodes`

> `kops validate cluster`

> `kubectl -n kube-system get po`

## Create dashbord
---

> `kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml`

__Execute the below command to find the admin users password:__

> `kops get secrets kube --type secret -oplaintext`

__Execute the below command to find the Kubernetes master hostname:__

> `kubectl cluster-info`

__Access the Kubernetes dashboard using the following URL__

> `https://<kubernetes-master-hostname>/ui`

__Provide the username as admin and the password obtained above at the step__

 > `kops get secrets kube --type secret -oplaintext  #get password admin`
 
 Execute the below command to find the admin service account token. Note the secret name used here is different from the previous one:

> `kops get secrets admin --type secret -oplaintext`

## Delete the Cluster

> `kops delete cluster --name ${NAME} --yes`


Use material from :

* https://github.com/kubernetes/kops/blob/master/docs/aws.md

* https://medium.com/containermind/how-to-create-a-kubernetes-cluster-on-aws-in-few-minutes-89dda10354f4
